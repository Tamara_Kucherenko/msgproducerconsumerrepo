﻿using Bogus;
using MongoDbRepository;
using MongoDbRepository.Models;
using System.Collections.Generic;

namespace FakeObjectsGenerator
{
    public class GeneratorOfFakeObjects
    {
        public DocumentObject Create()
        {
            var dict = new Dictionary<string, string>();
            for (int i = 0; i < 3; i++)
            {
                dict.Add("key"+i.ToString(), "value");
            }

            var attribute = new Faker<Attribute>()
                .RuleFor(a => a.ColumnName, f => f.Lorem.Word())
                .RuleFor(a => a.Value, f => f.Vehicle.Model())
                .RuleFor(a => a.ValueType, f => f.Vehicle.Fuel());

            var fakeModel = new Faker<DocumentModel>()
                .RuleFor(m => m.Action, f => f.PickRandom("Create", "None", "Update", "Delete"))
                .RuleFor(m => m.EntityName, f => f.Vehicle.Manufacturer())
                .RuleFor(m => m.Attributes, f => attribute.Generate(4).ToArray())
                .RuleFor(m => m.LinkAttributes, dict);

            var fakeDoc = new Faker<DocumentObject>()
                .RuleFor(d => d.UpdateDateTime, f => f.Date.Recent())
                .RuleFor(d => d.Models, f => fakeModel.Generate(2).ToArray());

            return fakeDoc.Generate();
        }

        public MessageModel createMessage()
        {
            var m = new Faker<MessageModel>()
                .RuleFor(m => m.Message, f => f.Lorem.Sentence(5));
            return m;
        }
    }
}
