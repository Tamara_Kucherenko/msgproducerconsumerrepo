﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using MongoDbRepository.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MongoDbRepository
{
    public class CustomMongoDbRepository
    {
        const string connectionString = "mongodb://localhost:27017/messageStore"; // адрес сервера
        IMongoCollection<MessageModel> Messages;
        IMongoCollection<DocumentObject> Documents;
        IMongoDatabase database;
        public CustomMongoDbRepository()
        {
            var connection = new MongoUrlBuilder(connectionString);
            // получаем клиента для взаимодействия с базой данных
            MongoClient client = new MongoClient(connectionString);
            // получаем доступ к самой базе данных
            database = client.GetDatabase(connection.DatabaseName);
            Messages = database.GetCollection<MessageModel>("Messages");
            Documents = database.GetCollection<DocumentObject>("Documents");
        }

        public async Task<IEnumerable<string>> getMessages()
        {
            var messages = await Messages.Find(new FilterDefinitionBuilder<MessageModel>().Empty).ToListAsync();
            var objects = await Documents.Find(new FilterDefinitionBuilder<DocumentObject>().Empty).ToListAsync();
            List<string> res = new List<string>();

            foreach(var m in messages) {
                res.Add(m.Message);
            }

            foreach(var ob in objects)
            {
                foreach(var model in ob.Models)
                {
                    res.Add(model.Action);
                }
            }

            return res;
        }

        public async Task Create(MessageModel m)
        {
            await Messages.InsertOneAsync(m);
        }

        public async Task Create(DocumentObject d)
        {
            await Documents.InsertOneAsync(d);

        }
    }
}
