﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;


namespace MongoDbRepository.Models
{
    public class DocumentModel
    {
        [BsonElement("action")]
        public string Action { get; set; }
        [BsonElement("entityName")]
        public string EntityName { get; set; }
        [BsonElement("attributes")]
        public Attribute[] Attributes { get; set; }
        [BsonElement("linkAttributes")]
        public Dictionary<string,string> LinkAttributes { get; set; }
    }
}
