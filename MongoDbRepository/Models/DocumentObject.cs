﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MongoDbRepository.Models
{
    public class DocumentObject
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("updateDateTime")]
        public DateTime UpdateDateTime { get; set; }
        [BsonElement("models")]
        public DocumentModel[] Models { get; set; }
    }
}
