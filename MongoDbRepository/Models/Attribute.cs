﻿using MongoDB.Bson.Serialization.Attributes;

namespace MongoDbRepository.Models
{
    public class Attribute
    {
        [BsonElement("columnName")]
        public string ColumnName { get; set; }
        [BsonElement("value")]
        public string Value { get; set; }
        [BsonElement("valueType")]
        public string ValueType { get; set; }
    }
}