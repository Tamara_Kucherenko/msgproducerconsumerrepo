﻿using MongoDbRepository;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using MongoDbRepository.Models;
using Messages_types;

namespace Messages_Consumer
{
    class Consumer
    {
        ConnectionFactory factory;
        IConnection rabbitMqConnection;
        IModel rabbitMqChannel;
        private const string CONFIG_QUEUE = "CustomQueue";
        readonly CustomMongoDbRepository mongodb;
        public Consumer()
        {
            factory = new ConnectionFactory()
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672"),
                DispatchConsumersAsync = true
            };

            rabbitMqConnection = factory.CreateConnection();
            rabbitMqChannel = rabbitMqConnection.CreateModel();

            //declare the queue  
            rabbitMqChannel.QueueDeclare(queue: CONFIG_QUEUE,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

            rabbitMqChannel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            mongodb = new CustomMongoDbRepository();
        }

        public void GetMessage()
        {
            //consume the message received  
            var consumer = new AsyncEventingBasicConsumer(rabbitMqChannel);
            consumer.Received += Consumer_Received_Object;
            consumer.Received += Consumer_Received_Message;
            rabbitMqChannel.BasicConsume(queue: CONFIG_QUEUE,
                    autoAck: true,
                    consumer: consumer);
        }

        private async Task Consumer_Received_Object(object sender, BasicDeliverEventArgs @event)
        {
            var message = Encoding.UTF8.GetString(@event.Body.ToArray());
            if(@event.BasicProperties.Type.Equals(Types.Object.ToString()))
            {
                var new_object = JsonConvert.DeserializeObject<DocumentObject>(message);
                await mongodb.Create(new_object);
            }
            
            await Task.Delay(2500);
        }

        private async Task Consumer_Received_Message(object sender, BasicDeliverEventArgs @event)
        {
            var message = Encoding.UTF8.GetString(@event.Body.ToArray());
            if (@event.BasicProperties.Type.Equals(Types.Message.ToString()))
                await mongodb.Create(new MessageModel() { Message = message });
            await Task.Delay(2500);
        }

        public void GetMessagesFromDb()
        {
            var messages = mongodb.getMessages();
            foreach (var m in messages.Result)
                Console.WriteLine(m);
        }
    }
}
