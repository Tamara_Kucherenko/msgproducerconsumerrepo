﻿using System;

namespace Messages_Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            Consumer c = new Consumer();
            c.GetMessage();
            c.GetMessagesFromDb();
            Console.ReadLine();
        }
    }
}
