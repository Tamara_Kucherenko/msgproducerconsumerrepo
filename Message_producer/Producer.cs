﻿

using Messages_types;
using MongoDbRepository.Models;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Text;

namespace Message_producer
{
    class Producer
    {
        ConnectionFactory factory;
        IModel channel;
        private const string CONFIG_QUEUE = "CustomQueue";
        private IBasicProperties properties;
        public Producer()
        {
            factory = new ConnectionFactory()
            {
                HostName = "localhost",
                UserName = ConnectionFactory.DefaultUser,
                Password = ConnectionFactory.DefaultPass,
                Port = AmqpTcpEndpoint.UseDefaultPort
            };

            channel = factory.CreateConnection().CreateModel();
            channel.QueueDeclare(queue: CONFIG_QUEUE,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
            properties = channel.CreateBasicProperties();
        }

        public void SendMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            properties.Type = Types.Message.ToString();
            channel.BasicPublish(exchange: "",
                routingKey: CONFIG_QUEUE,
                basicProperties: properties,
                body: body);
        }

        public void SendMessage(DocumentObject obj)
        {
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj));
            properties.Type = Types.Object.ToString();
            channel.BasicPublish(exchange: "",
                routingKey: CONFIG_QUEUE,
                basicProperties: properties,
                body: body);
        }
    }
}
