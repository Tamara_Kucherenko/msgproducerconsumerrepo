﻿using FakeObjectsGenerator;
using System;
using System.Threading;

namespace Message_producer
{
    class Program
    {
        static void Main(string[] args)
        {
            var producer = new Producer();
            var rand = new Random();
            var objectCreator = new GeneratorOfFakeObjects();
            while (true)
            {
                var flag = rand.Next(0, 2);
                if (flag == 1)
                    producer.SendMessage(objectCreator.Create());
                else
                    producer.SendMessage(objectCreator.createMessage().Message);
                Thread.Sleep(5000);
            }
            
        }
    }
}
